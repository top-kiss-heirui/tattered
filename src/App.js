import { BrowserRouter } from 'react-router-dom';
import Home from './views/home/index'

function App() {
  return (
    <BrowserRouter>
      
      <Home></Home>
    </ BrowserRouter>
  );
}

export default App;
