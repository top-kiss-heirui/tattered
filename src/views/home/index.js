import {
  LaptopOutlined,
  NotificationOutlined,
  UserOutlined,
} from "@ant-design/icons";
import {
  useHistory,
  Route,
  withRouter,
} from "react-router-dom";
import { Breadcrumb, Layout, Menu, theme } from "antd";
import React from "react";
import "../../styles/home.less";

const { Header, Content, Sider } = Layout;
const items1 = ["1", "2", "3", "4"].map((key) => ({
  key,
  label: `标签 ${key}`,
}));

const list = [
  {
    icon: UserOutlined,
    name: "首页",
    children: [{ icon: UserOutlined, name: "登录", path: "/login" }],
  },
  {
    icon: LaptopOutlined,
    name: "个人中心",
    children: [
      { icon: UserOutlined, name: "修改密码", path: "/changePass" },
      { icon: UserOutlined, name: "个人中心", path: "/userCenter" },
    ],
  },
  {
    icon: NotificationOutlined,
    name: "用户管理",
    children: [{ icon: UserOutlined, name: "用户", path: "/user" }],
  },
  { icon: NotificationOutlined, name: "商品类型管理", children: [{ icon: UserOutlined, name: "商品类型", path: "/comType" }] },
  { icon: NotificationOutlined, name: "商品信息管理", children: [{ icon: UserOutlined, name: "商品信息", path: "/comInfo" }] },
  { icon: NotificationOutlined, name: "系统管理",children: [{ icon: UserOutlined, name: "系统信息", path: "/system" }] },
  { icon: NotificationOutlined, name: "订单管理",children: [{ icon: UserOutlined, name: "订单信息", path: "/order" }] }
];

const items2 = list.map((item, index) => {
  return {
    key: `sub ${index}`,
    icon: React.createElement(item.icon),
    label: item.name,
    children: item.children?.map((item1) => {
      return {
        icon: React.createElement(item1.icon),
        key: item1.path,
        label: item1.name,
      };
    }),
  };
});

const Home = () => {
  const history = useHistory();
  const {
    token: { colorBgContainer },
  } = theme.useToken();
  return (
    <Layout>
      <Header className="header">
        <div className="logo" />
        <Menu
          theme="dark"
          mode="horizontal"
          defaultSelectedKeys={["2"]}
          items={items1}
        />
      </Header>
      <Layout>
        <Sider
          width={200}
          style={{
            background: colorBgContainer,
          }}
        >
          <Menu
            mode="inline"
            defaultSelectedKeys={["1"]}
            defaultOpenKeys={["首页"]}
            style={{
              height: "100%",
              borderRight: 0,
            }}
            items={items2}
            onClick={(e) => {
              // console.log(history.location);
              history.push(e.key);
            }}
          />
        </Sider>
        <Layout
          style={{
            padding: "0 24px 24px",
          }}
        >
          <Breadcrumb
            style={{
              margin: "16px 0",
            }}
          >
            <Breadcrumb.Item>Home</Breadcrumb.Item>
            <Breadcrumb.Item>List</Breadcrumb.Item>
            <Breadcrumb.Item>App</Breadcrumb.Item>
          </Breadcrumb>
          <Content
            style={{
              padding: 24,
              margin: 0,
              minHeight: 280,
              background: colorBgContainer,
            }}
          >
            <Route
              path="/login"
              render={() => {
                return <div>登录</div>;
              }}
            ></Route>
          </Content>
        </Layout>
      </Layout>
    </Layout>
  );
};
export default withRouter(Home);
